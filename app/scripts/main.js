(function($) {
  $.fn.shorten = function(settings) {

    var config = {
      showChars: 100,
      ellipsesText: '...',
      moreText: 'more',
      lessText: 'less'
    };

    if (settings) {
      $.extend(config, settings);
    }

    $(document).off('click', '.morelink');

    $(document).on({
      click: function() {

        var $this = $(this);
        if ($this.hasClass('less')) {
          $this.removeClass('less');
          $this.html(config.moreText);
        } else {
          $this.addClass('less');
          $this.html(config.lessText);
        }
        $this.parent().prev().slideToggle('slow');
        $this.prev().slideToggle('slow');
        return false;
      }
    }, '.morelink');

    return this.each(function() {
      var $this = $(this);
      if ($this.hasClass('shortened')) return;

      $this.addClass('shortened');
      var content = $this.html();
      if (content.length > config.showChars) {
        var c = content.substr(0, config.showChars);
        var h = content.substr(config.showChars, content.length - config.showChars);
        var html = c + '<span class="moreellipses">' + config.ellipsesText + ' </span><span class="morecontent"><span>' + h + '</span> <a href="#" class="morelink">' + config.moreText + '</a></span>';
        $this.html(html);
        $('.morecontent span').hide();
      }
    });

  };

})(jQuery);

$(document).ready(function() {

  $('#nav-toggle').click(function() {
    $('.main-header__nav').slideToggle('fast');
  });

  $('.main-slider').slick({
    dots: true,
    arrows: false,
    autoplay: true,
    infinite: true,
    speed: 300
  });

  /*$('.main-features-body__card-text').readmore({
    speed: 500,
    collapsedHeight: 50,
    moreLink: '<a href="#">Развернуть</a>',
    lessLink: '<a href="#">Развернуть</a>'
  });*/

  $('.main-features-body__card-text').shorten({
    'showChars': 490,
    'moreText': 'Развернуть',
    'lessText': 'Свернуть'
  });

  $('a[href="#s1"]').click(function() {
    $.scrollTo('#s1', 500, {
      offset: -100
    });
  });

  $('a[href="#s2"]').click(function() {
    $.scrollTo('#s2', 500, {
      offset: -100
    });
  });

  $('a[href="#s3"]').click(function() {
    $.scrollTo('#s3', 500, {
      offset: -100
    });
  });

  $('a[href="#s4"]').click(function() {
    $.scrollTo('#s4', 500, {
      offset: -100
    });
  });

  $('a[href="#s5"]').click(function() {
    $.scrollTo('#s5', 500, {
      offset: -100
    });
  });

  $('a[href="#s6"]').click(function() {
    $.scrollTo('#s6', 500, {
      offset: -100
    });
  });

  $('a[href="#s7"]').click(function() {
    $.scrollTo('#s7', 500, {
      offset: -100
    });
  });

  $('a[href="#s8"]').click(function() {
    $.scrollTo('#s8', 500, {
      offset: -100
    });
  });

});

new WOW().init();